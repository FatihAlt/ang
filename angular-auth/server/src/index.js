const express = require("express");
const PORT = process.env.PORT || 4000;
const morgan = require("morgan");
const cors = require("cors");
const bodyParser = require("body-parser");

const app = express();

app.use(cors()); 


require("./config/db")(app);

app.use(bodyParser.urlencoded({
    extended: false
}));
app.use(bodyParser.json());


app.use(morgan("dev")); 

const userRoutes = require("./account/userRoute"); 
app.use("/user", userRoutes);

app.get("/", (req, res) => {
    console.log("yy?");
});

app.listen(PORT, () => {
    console.log(`App is running on ${PORT}`);
});