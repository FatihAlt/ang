const mongoose = require('mongoose');
const Schema = mongoose.Schema;

// Define collection and schema
let profile = new Schema({
   age: {
      type: Number
   },
   famille: {
      type: String
   },
   race: {
      type: String
   },
   nourriture: {
      type: String
   }
}, {
   timestamps: true,
});

module.exports = mongoose.model("User", userSchema);
userSchema.plugin(uniqueValidator, {
    message: '{PATH} Already in use'
});